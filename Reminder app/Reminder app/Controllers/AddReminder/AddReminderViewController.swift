//
//  AddReminderViewController.swift
//  Reminder app
//
//  Created by Sajeev ps on 24/06/21.
//

import UIKit

class AddReminderViewController: UIViewController {

    @IBOutlet weak private var viewAddNote: ViewAddReminder!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction private func didTapDoneBtn(_ sender: Any) {
        if self.viewAddNote.validateFields(){
            self.viewAddNote.addReminder()
            self.showAlert(title: Alert.AddReminder.success, message: Alert.AddReminder.successMsg) { (index) in
                NotificationCenter.shared.addReminderNotification()
                self.navigationController?.popViewController(animated: true)
            }
        }
        else {
            self.showAlert(title: Alert.AddReminder.warning, message: Alert.AddReminder.validationError, completion: nil)
        }
    }
}


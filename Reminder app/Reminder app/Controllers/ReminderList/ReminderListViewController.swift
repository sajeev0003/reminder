//
//  ReminderListViewController.swift
//  Reminder app
//
//  Created by Sajeev ps on 23/06/21.
//

import UIKit

class ReminderListViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!

    private var reminder: [Reminder] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.tblView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.fetchProductsFromDB()
    }
    
    func fetchProductsFromDB()  {
        if let reminder = DataBaseManager().readAllReminder() {
            self.reminder.removeAll()
            self.reminder.append(contentsOf: reminder)
            self.tblView.reloadData()
        }
        self.tblView.isHidden = self.reminder.isEmpty
    }
}

//MARK:- UITableView
extension ReminderListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reminder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReminderCell.identifier, for: indexPath) as? ReminderCell
        let reminder = self.reminder[indexPath.row]
        cell?.configureCell(reminder)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let reminder = self.reminder[indexPath.row]
        reminder.isCompleted = !reminder.isCompleted
        self.reminder[indexPath.row] = reminder
        CoreData.sharedCoreData.saveContext()
        NotificationCenter.shared.addReminderNotification()
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let reminder = self.reminder[indexPath.row]
            self.reminder.remove(at: indexPath.row)
            DataBaseManager().deleteReminder(reminder)
            tableView.deleteRows(at: [indexPath], with: .fade)
            NotificationCenter.shared.addReminderNotification()
        }
    }
}

//
//  SplashViewController.swift
//  Reminder app
//
//  Created by Sajeev ps on 23/06/21.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.fadeIn()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func fadeIn() {
        UIView.animate(withDuration: 0.1) {
            self.imageView.alpha = 1
        } completion: { (completed) in
            self.fadeOut()
        }
    }
    
    func fadeOut()  {
        UIView.animate(withDuration: 0.5) {
            self.imageView.alpha = 0
        } completion: { (completed) in
            self.performSegue(withIdentifier: Segue.splashToDashboard, sender: self)
        }
    }
}


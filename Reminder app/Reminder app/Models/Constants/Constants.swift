//
//  Constants.swift
//  Reminder app
//
//  Created by Sajeev ps on 23/06/21.
//

import Foundation

struct Segue {
    static let splashToDashboard     = "splashToDashboard"
}

struct Alert {

    struct AddReminder {
        static let warning           = "Warning"
        static let validationError   = "Please fill all the fields"
        static let success           = "Wow.."
        static let successMsg        = "Task have been successfully added"
    }
    
    struct AlertButton {
        static let ok                = "Ok"
    }
}

struct DateFormat {
    static let timeFormat            = "yyyy-MM-dd HH:mm:ss"
}

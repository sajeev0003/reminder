//
//  DataBaseManager.swift
//  Reminder app
//
//  Created by Sajeev ps on 24/06/21.
//

import Foundation
import CoreData

class DataBaseManager {
    static let tableName = "Reminder"
    
    func insertReminder(name: String, note: String, date: Date)  {
        let context = CoreData.sharedCoreData.persistentContainer.viewContext
        let reminder = self.insertRecord(DataBaseManager.tableName, context: context) as? Reminder
        reminder?.name = name
        reminder?.note = note
        reminder?.date = date
        reminder?.isCompleted = false
        CoreData.sharedCoreData.saveContext()
    }
    
    func deleteReminder(_ reminder: Reminder)  {
        let context = CoreData.sharedCoreData.persistentContainer.viewContext
        context.delete(reminder)
        CoreData.sharedCoreData.saveContext()
    }
    
    func readAllReminder() -> [Reminder]? {
        let context = CoreData.sharedCoreData.persistentContainer.viewContext
        return self.readRecords(fromCoreData: DataBaseManager.tableName, context: context) as? [Reminder]
    }
    
    func readRecords(fromCoreData table: String, context: NSManagedObjectContext) -> [Any] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entity = NSEntityDescription.entity(forEntityName: table, in: context)
        fetchRequest.entity = entity
        fetchRequest.returnsObjectsAsFaults = false
        let records: [Any]? = try? context.fetch(fetchRequest)
        return records!
    }
    
    func insertRecord(_ table: String, context: NSManagedObjectContext) -> Any? {
        return NSEntityDescription.insertNewObject(forEntityName: table, into: context)
    }
}

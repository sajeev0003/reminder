//
//  Extension.swift
//  Reminder app
//
//  Created by Sajeev ps on 24/06/21.
//

import UIKit

extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
}

extension UIViewController {
    func showAlert(title: String = "", message: String, options: [String]? = [Alert.AlertButton.ok], completion: ((Int) -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if let options = options {
            for (index, option) in options.enumerated() {
                alertController.addAction(UIAlertAction(title: option, style: .default, handler: { (action) in
                    completion?(index)
                }))
            }
        }
        self.present(alertController, animated: true, completion: nil)
    }
}

extension Date {
    func convertToString(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}

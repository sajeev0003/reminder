//
//  NotificationCenter.swift
//  Reminder app
//
//  Created by Sajeev ps on 24/06/21.
//

import UIKit
import UserNotifications

class NotificationCenter: NSObject {
    
    static let shared = NotificationCenter()
    
    func addReminderNotification()  {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        if let reminders = DataBaseManager().readAllReminder() {
            for reminder in reminders {
                if !reminder.isCompleted {
                    self.sendNotification(title: reminder.name ?? "", subtitle: "", body: reminder.note ?? "", badge: 1, delayInerval: reminder.date)
                }
            }
        }
    }
    
    func sendNotification(title:String,
                          subtitle:String,
                          body:String,
                          badge:Int?,
                          delayInerval:Date?){
        //setting notification content
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = title
        notificationContent.subtitle = subtitle
        notificationContent.body = body
        
        //when we want trigger the notification
        var trigger: UNCalendarNotificationTrigger?
        
        if let delayInterval = delayInerval{
            let dateComponent = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: delayInterval)
            trigger = UNCalendarNotificationTrigger(dateMatching: dateComponent, repeats: false)
        }
        if let badge = badge{
            var currentBadgeCount = UIApplication.shared.applicationIconBadgeNumber
            currentBadgeCount += badge
            notificationContent.badge = NSNumber(integerLiteral: currentBadgeCount)
        }
        
        notificationContent.sound = UNNotificationSound.default
        
        UNUserNotificationCenter.current().delegate = self
        
        //combine content triger to single object
        let request = UNNotificationRequest(identifier: "TestLocalNotification", content: notificationContent, trigger: trigger)
        
        //add notification request to NotificationCenter
        UNUserNotificationCenter.current().add(request) { (error) in
            if let error = error{
                print(error.localizedDescription)
            }
        }
    }
}

//MARK:- UNUserNotificationCenterDelegate
extension NotificationCenter: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
           completionHandler([.banner, .sound])
       }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let identifier = response.actionIdentifier
        switch identifier{
        case UNNotificationDismissActionIdentifier:
            print("The notification was dissmissed")
            completionHandler()
        case UNNotificationDefaultActionIdentifier:
            print("The user opened the app from the notification ")
        default:
            print("the default case was called")
            completionHandler()
        }
    }
}

//
//  ReminderCell.swift
//  Reminder app
//
//  Created by Sajeev ps on 23/06/21.
//

import UIKit

class ReminderCell: UITableViewCell {
    
    static let identifier = "reminderCell"
    
    @IBOutlet weak var lblReminderName: UILabel!
    @IBOutlet weak var lblReminderDate: UILabel!

    func configureCell(_ item: Reminder)  {
        self.accessoryType = (item.isCompleted) ? .checkmark : .none
        self.lblReminderName.text = item.name
        self.lblReminderDate.text = item.date?.convertToString(format: DateFormat.timeFormat)
    }
    
}

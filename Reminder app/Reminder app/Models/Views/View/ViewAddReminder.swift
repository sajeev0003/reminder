//
//  ViewAddReminder.swift
//  Reminder app
//
//  Created by Sajeev ps on 24/06/21.
//

import UIKit

class ViewAddReminder: UIView {
    
    @IBOutlet weak private var txtName: UITextField!
    @IBOutlet weak private var txtNote: UITextView!
    @IBOutlet weak private var datePicker: UIDatePicker!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.datePicker.minimumDate = Date()
    }    
}

//MARK:- public Method
extension ViewAddReminder {
    
    func validateFields() -> Bool {
        return !((self.txtName.text?.isEmpty ?? false) ||
            (self.txtNote.text?.isEmpty ?? false))
    }
    
    func addReminder() {
        self.txtName.resignFirstResponder()
        self.txtNote.resignFirstResponder()
        DataBaseManager().insertReminder(name: self.txtName.text ?? "", note: self.txtNote.text ?? "", date: datePicker.date)
    }
}


//MARK:- UITextFieldDelegate
extension ViewAddReminder: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.txtName {
            self.txtNote.becomeFirstResponder()
        }
        textField.resignFirstResponder()
        return true
    }
}
//MARK:- UITextViewDelegate
extension ViewAddReminder: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
}
